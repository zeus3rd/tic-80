## Details

All .lua files in this repository are written in moonscript for minimalism

## Installing Lua, LuaRocks and Moonscript

MacOSX

```
brew update
brew install lua
luarocks install moonscript
```

# Testing Scripts

```
moon ./scripts/actor.bats.lua
```

## Building Template Projects

Dynamic scripts for building files in projects/** rely on NodeJS. This is primarily for use in Gitlab build pipelines but can be built locally using the following command:

```
npm run build
```

## Live Building

To accurately keep all projects up to date, the following command will watch your project/script folders for any changes and rebuild your project output files on every change:

```
npm run watch
```
