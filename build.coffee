fs = require 'fs'
path = require 'path'
shell = require 'shelljs'

projects = path.resolve __dirname,'projects'
scripts = path.resolve __dirname,'scripts'

patternScriptLocation = ///
  --\[\[INSERT\sSCRIPT:   # Start of script block
  \s*(.+)*                # Source Filename
  --\]\]                  # End of opening tag
  [\w\d\s\S]*             # Any Internal Content
  --\[\[END\sSCRIPT--\]\] # Closing tag
  |_                      # OR Operator
  ///gi                   # Global, ignore case
patternStartScript = /--\[\[INSERT SCRIPT:\s*(.+)*--\]\]/gi

patternTop = /--\sSTARTSCRIPT(.|\n)*?--\sENDSCRIPT/g
patternFile = /--\sFILE:\s(.)*?\n/g
patternFileData = /--\sSTARTCODE(.|\n)*?--\sENDCODE/g

# Walk through all child files and folders
walkSync = (dir,filelist=[]) ->
  files = fs.readdirSync dir
  files.forEach (file) ->
    if fs.statSync(path.resolve dir,file).isDirectory()
      filelist = walkSync path.resolve(dir,file),filelist
    else if file.indexOf('.template.lua') <= 0
      # Ignore file
    else
      filelist.push path.resolve dir,file
  filelist

projectFiles = walkSync path.resolve projects

for proj in projectFiles
  data = fs.readFileSync proj
  data = data.toString()

  replaceBlocks = []

  matches = data.match patternTop
  if !!matches && matches.length
    for section in data.match patternTop
      file = section.match patternFile
      filepath = file[0].replace '-- FILE: ', ''
      filepath = filepath.replace '\n',''
      fileData = fs.readFileSync path.resolve scripts,filepath
      fileData = fileData.toString()
      fileData = (fileData.match patternFileData)[0]

      replaceBlocks.push
        src: section
        target: """
        -- STARTSCRIPT\n\
        -- FILE: #{filepath.toString()}\n\
        #{fileData}\n\
        -- ENDSCRIPT
        """

    for block in replaceBlocks
      data = data.replace block.src,block.target

    destFile = proj.replace('.template','')
    # console.log "Writing file #{proj} -> #{destFile}"
    console.log "#{destFile}"
    err = fs.writeFileSync destFile,data
    if err then console.error err

    if shell.which 'moon'
      # console.log "Testing #{destFile}"
      shell.exec "moon #{destFile}"
   
  


    # data.replace patternScriptLocation, 'SOMETHING!!!!'
    # /TEST/g.exec data, -> console.log 'TRIGGERED!'
    # console.log data.replace /TEST/g, 'FUBAR'
    # # data.replace /TEST/, 'FUBAR'
    # console.log 'data',data
    # # test = patternScriptLocation.exec data
    # # console.log 'test',test