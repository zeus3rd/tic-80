-- Build a sprite from text
-- https://github.com/nesbox/TIC-80/wiki/spr#example
-- data = String of characters (0-f?) to convert into 8x8 image

-- STARTCODE
sprite=(data,loc=1)=>
	-- TODO: Memory location logic
	-- This version draws to sprite location "1". Possibly (0x4000+32*loc+i) ?
	for i=0,31 do
		poke 0x4000+32+i, tonumber string.sub data, i*3+1, i*3+3

-- ENDCODE