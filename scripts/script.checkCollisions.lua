-- script: moon
-- status: untested
-- ref: https://gamedev.stackexchange.com/questions/45141/html5-platformer-collision-detection-problem


-- >>> Expected Structure
-- class Actor:
	-- x=?
	-- y=?
	-- vx=?
	-- vy=?
	-- width=?
	-- height=?
-- 
-- class Tile:
	-- x=?
	-- y=?
	-- width=?
	-- height=?

-- >>> Process
-- 1. Move Actor
-- 2. Loop through each tile within character collision bounds
-- 3. Run checkCollisions() between Actor and each Tile

-- STARTCODE
distanceBetween=(a,b)-> return Math.abs b-a

handleCollisions=(a,b)->
	a_top=a.y
	a_bottom=a.y+a.height
	a_left=a.x
	a_right=a.x+a.width
	b_top=b.y
	b_bottom=b.y+b.height
	b_left=b.x
	b_right=b.x+b.width

	if a_bottom+a.vy>b_top and distanceBetween(a_bottom, b_top)+a.vy<distanceBetween(a_bottom, b_bottom)
		a.topCollision=true
		a.y=b.y-a.height+2
		a.vy=0
		a.canJump=true
	else if a_top+a.vy<b_bottom and distanceBetween(a_top, b_bottom)+a.vy<distanceBetween(a_top, b_top)
		a.bottomCollision = true
		a.y = b.y + b.height
		a.vy = 0
	else if a_right+a.vx>b_left and distanceBetween(a_right, b_left)<distanceBetween(a_right, b_right)
		a.rightCollision = true
		a.x = b.x - a.width - 3
		--a.vx = 0
	else if a_left+a.vx<b_right and distanceBetween(a_left, b_right)<distanceBetween(a_left, b_left)
		a.leftCollision = true
		a.x = b.x + b.width + 3
		--a.vx = 0

checkCollisions=(a,b)->
	if a.x>b.x+b.width or a.x+a.width<b.x or a.y>b.y+b.height or a.y+a.height<b.y
		return false
	handleCollisions(a,b)
	return true
-- ENDCODE

print "Success!"