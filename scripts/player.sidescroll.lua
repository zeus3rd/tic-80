-- x/y = Position
-- xv/yv = Velocity
-- t = Frame Timer (incremental)
-- f = Frame (Currently)
-- w = width
-- h = height

-- * FUNCTIONS (public)
-- pos = Set player position quickly
-- step = Should be called in every TIC()
-- setState = (state,vX,vY), nil params don't change values
-- * FUNCTIONS (private)
-- draw
-- physics = May be moved to physics.sidescroll.lua

-- * STATES
-- These can be collapsed into
-- player.states=
-- idle:{c:0,...}

-- c = Colorkey
-- f = Flip
-- t = Time between frame changes
-- s = List of sprites
-- sc = Scale (At least 1)
-- r = rotate (0,1,2,3)
-- next = Next state to move to after all sprites are drawn


-- STARTCODE
player=
	x:0
	y:0
	w:8
	h:8
	vX:0
	vY:0
	t:1
	f:1
	state:'idle'
	states:
		idle:
			c:0
			f:0
			t:40
			s:{256}
			next:'idle'
	pos:(x,y)=>
		@x=x
		@y=y
	setState:(s,vX,vY)=>
		@state=s or @state
		@vX=vX or @vX
		@vY=vY or @vY
	-- jump:=>
	-- 	-- TODO: Jump directly upwards, higher
	-- hop:(dir=1)=>
	-- 	if @state!="idle" then return
	-- 	@state="airborne"
	-- 	@d=dir
	-- 	@vY=-4
	-- 	@vX=dir*1

	step:=>
		@move!
		-- @physics!
		@draw!
	move:=>
		-- if @state=='airborne'
		-- 	if @vY >4 then @vY=4
		@x+=@vX
		@y+=@vY

		-- Tile Above

		-- @vY+=1 -- Gravity Acceleration
	-- physics:=>
	-- 	tx=math.floor (@x/8)
	-- 	ty=math.floor (@y/8)
	-- 	if isSolid~=nil
	-- 		-- Vertical
	-- 		if isSolid tx,ty+1
	-- 			@state="idle"
	-- 			@x=tx*8
	-- 			@y=ty*8
	-- 			@vY=0
	-- 			@vX=0
	-- 			if @d<0 then @x+=1
	-- 			return
	-- 		else @state="airborne"

	-- 		-- if isSolid tx,ty then @vY=0

	-- 		-- Horizontal
	-- 		if @d==1 then tx+=(@w/8)
	-- 		if isSolid tx,ty
	-- 			@state="idle"
	-- 			@vX=0
	draw:=>
		st=@states[@state]
		if st~=nil
			c=st.c or 0
			s=st.s[@f] or 256
			sc=st.sc or 1
			r=st.r or 0
			f=st.f or 0
			t=st.t or 10
			n=st.next or @state or 'idle'
			w=1
			h=1
			-- Draw
			-- TODO: Draw relevant to map scroll pos if applicable (and do-able)
			if spr~=nil then spr s,@x,@y,c,sc,f,r,w,h
			-- Next Frame/State
			@t+=1
			if @t>t
				@f+=1
				@t=1
			if @f>#st.s
				@f=1
				@state=n
-- ENDCODE


-- Usage Example:

player\pos 10,10
player.states=
	idle:
		c:0
		t:3
		s:{256,257}
	idle_l:
		c:0
		f:1
		t:3
		s:{256,257}
	walk:
		c:0
		t:3
		s:{258,259}
	walk_l:
		c:0
		t:3
		s:{258,259}
	jump:
		c:0
		t:3
		s:{260}
	jump_l:
		c:0
		f:1
		t:3
		s:{260}

player.state='walk_l'

print 'Setting state >>'
player\setState 'walk_l',-1
player\setState 'walk_l',-1
player\setState 'walk_l',-1
player\setState 'walk_l',-1
player\setState 'walk_l',-1
player\setState 'walk_l',-1

print 'Draw >>'
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!
player\draw!

print 'Move >>'
player\move!
player\move!
player\move!
player\move!

print 'Step >>'
player\step!
player\step!
player\step!
player\step!
player\step!
player\step!
player\step!
player\step!
player\step!

print 'Success!'