-- Adapted from https://github.com/nesbox/TIC-80/wiki/code-examples-and-snippets#shake-screen
-- Example
-- shake.start 4,30
	-- i = Itensity
	-- d = Duration (in frames)

-- STARTCODE
shake=
	time:0
	dur:1
	intense:0
	activate: false
	start:(i=4,d=1)=>
		@time=0
		@dur=d
		@intense=i
		@activate=true
	step:=>
		if @activate~=true then return
		if @time>=@dur
			@time=0
			@activate=false
			memset(0x3FF9,0,2)
			return
		poke(0x3FF9,math.random(-intense,intense))
		poke(0x3FF9+1,math.random(-intense,intense))
		@time+=1
-- ENDCODE