-- oX/oY = X/Y-Coordinate Offset
-- tX/tY = X/Y tolerance (dead-zone) player may move from center screen before map offsets to follow them
-- fX/fY = X/Y direction offset, when player faces a direction, move the map this far ahead of the player to keep more of the map infront of the player visible
-- w/h = Width/Height of tiles to draw

-- step! = Function to be called within TIC()
-- init(oX,oY,tX,tY) = Function to be called on level initiation

-- STARTCODE
worldMap=
	oX:0
	oY:0
	tX:20
	tY:20
	fX:20
	fY:20
	w:1
	h:1
	init:(oX=0,oY=0,tX=20,tY=20,fX=20,fY=20)=>
		@oX=oX
		@oY=oY
		@tX=tX
		@tY=tY
		@fX=fX
		@fY=fY
	move:=>
		-- TODO: Check player position and run global function to keep everything in focus
	draw:=>
		x=@oX
		y=@oY
		if map~=nil
			map x,y
	step:=>
		@move!
		@draw!
-- ENDCODE