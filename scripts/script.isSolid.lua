solidTileRanges={
	{1,31},
}
-- STARTCODE
isSolid=(x,y)->
	s=0
	if mget~=nil
		s=mget x,y
	for i,sprites in pairs solidTileRanges
		if s>=sprites[1] and s<=sprites[2] then return true
		return false
-- ENDCODE


-- END OF CODE
-- DON'T COPY BELOW THIS LINE (Test Code)

-- Test Scripts
print isSolid 1,1
print "Success!"