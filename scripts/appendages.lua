-- This script is for drawing extra body parts behind a player character model
-- Inspired by Tails in Sonic 2/3

-- State Attributes:
-- c = Colorkey
-- f = Flip
-- t = Time between frame changes
-- s = List of sprites
-- sc = Scale (At least 1)
-- r = rotate (0,1,2,3)
-- x = Offset x (from player.x)
-- y = Offset y (from player.y)

-- STARTCODE
appendages=
	items:{}
	add:(item={})=>
		-- Enforce default values
		item.target=item.target or player or {x:0,y:0}
		item.states=item.states or {}
		item.f=item.f or 1
		item.t=item.t or 0
		for i,o in pairs item.states
			o.x=o.x or 0
			o.y=o.y or 0
			o.c=o.c or 0
			o.r=o.r or 0
			o.f=o.f or 0
			o.t=o.t or 60
			o.sc=o.sc or 1
			o.s=o.s or {511}
		table.insert @items,item
	clear:=>
		for i in pairs @items
			@items[i]=nil
	draw:=>
		for i,o in pairs @items
			if o.target~=nil and o.target.state~=nil
				st=o.states[o.target.state]
				-- Check for state changes
				-- TODO: This doesn't yet work
				-- if o.laststate~=o.target.state
				-- 	o.laststate=o.target.state
					-- o.f=0
					-- o.t=0
				-- Draw/Animate
				if st~=nil
					x=o.target.x+st.x
					y=o.target.y+st.y
					c=st.c or 0
					s=st.s[o.f] or 511
					sc=st.sc or 1
					r=st.r or 0
					f=st.f or 0
					t=st.t or 10
					n=st.next or @state or 'idle'
					w=1
					h=1
					if spr~=nil then spr s,x,y,c,sc,f,r,w,h
					-- Next Frame
					o.t+=1
					if o.t>t
						o.f+=1
						o.t=1
					if o.f>#st.s
						o.f=1
						o.state=n
	step:=>
		@draw!
-- ENDCODE

-- Example use:
appendages\add
	target:player or {x:0,y:0,state:'idle'}
	states:
		idle:
			x:0
			y:0
			c:0
			r:0
			f:0
			t:60
			sc:1
			s:{288}
		idle_l:
			x:0
			y:0
			c:0
			r:0
			f:1
			t:60
			sc:1
			s:{288}

appendages\step!
appendages\step!
appendages\step!