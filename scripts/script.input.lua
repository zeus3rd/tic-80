-- Declare any these functions to handle input triggers and connect to desired effects
-- input_u
-- input_d
-- input_l
-- input_r
-- input_a
-- input_b

-- STARTCODE
input=
	-- Configuration:
	u:=>if input_u~=nil then input_u!
	d:=>if input_d~=nil then input_d!
	l:=>if input_l~=nil then input_l!
	r:=>if input_r~=nil then input_r!
	a:=>if input_a~=nil then input_a!
	b:=>if input_b~=nil then input_b!
	none:=>if input_none~=nil then input_none!
	step:=>
		if btnp~=nil
			if btnp 0,0,1 then @u!
			if btnp 1,0,1 then @d!
			if btnp 2,0,1 then @l!
			if btnp 3,0,1 then @r!
			if btnp 4,0,1 then @a!
			if btnp 5,0,1 then @b!
			n=false
			for i=0,5 do
				n=n or btn i
			if n==false then @none!
-- ENDCODE

input\u!
input\d!
input\l!
input\r!
input\a!
input\b!
input\none!
input\step!
print "Success!"