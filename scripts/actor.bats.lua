-- Dependencies:
-- script.isSolid

-- STARTCODE
bats=
	w:8
	h:8
	vJump:-60
	items:{}
	-- TODO: Migrate to named states method
	states:{
		{s:320}, -- Flap up
		{s:321,t:10}, -- Flap down
	}
	-- x/y = position
	-- s = @states index
	-- t = timer countdown
	-- d = direction (1/-1)
	add:(x=0,y=0,s=1,t=math.random(1,20),d=1)=>
		item={x:x,y:y,s:s,t:t,d:d,vX:60,vY:20}
		table.insert @items,item
	step:=>
		for i,o in pairs @items
			if o.vY>=60 then @flap o
			@move o
			@animate o
			@collide o
			@draw o

	flap:(o)=>
		o.s=2
		-- o.vY=@vJump
		o.vY=o.vY*-1
		o.vY+=math.random(0,100)-50
	move:(o)=>
		-- o.vY+=9.8/60 -- Gravital Acceleration
		o.vY+=6
		o.x+=(o.vX/60)*o.d
		o.y+=o.vY/60
	animate:(o)=>
		st=@states[o.s]
		if st.t~=nil then o.t-=1
		if o.t<=0
			o.s=1
			if o.s>#@states then o.s=1
			if st.t~=nil then o.t=st.t
	collide:(o)=>
		-- TODO: Player collision
		-- Map TileaCollision
		tx=math.floor (o.x/8)
		ty=math.floor (o.y/8)
		if isSolid~=nil
			-- Vertical
			if o.vY>0
				if isSolid tx,ty+1 then @flap o
			else if o.vY<0
				if isSolid tx,ty then o.vY=0
			-- Horizontal
			if o.d==1 then tx+=(@w/8)
			if isSolid tx,ty then o.d=o.d*-1
	draw:(o)=>
		st=@states[o.s]
		flip=0
		if o.d==-1 then flip=1
		if spr~=nil then spr st.s,o.x,o.y,0,1,flip
-- ENDCODE



	-- END OF CODE
	-- DON'T COPY BELOW THIS LINE (Test Code)
	debug:=>
		print

-- Test Scripts
bats\add 20,20
bats\add 40,20
print "Bat Count",#bats.items
bats\step!
bats\debug!
print "Success!"