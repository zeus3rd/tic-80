-- Redraw color palette items (0-15) using RGB (0-255)

-- STARTCODE
pal=(i,r,g,b)->
	if i<0 then i=0
	if i>15 then i=15
	if peek==nil then return
	if r==nil and g==nil and b==nil
		return peek(0x3fc0+(i*3)),peek(0x3fc0+(i*3)+1),peek(0x3fc0+(i*3)+2)
	else
		if r==nil or r<0 then r=0
		if g==nil or g<0 then g=0
		if b==nil or b<0 then b=0
		if r>255 then r=255
		if g>255 then g=255
		if b>255 then b=255
		poke 0x3fc0+(i*3)+2,b
		poke 0x3fc0+(i*3)+1,g
		poke 0x3fc0+(i*3),r
-- ENDCODE