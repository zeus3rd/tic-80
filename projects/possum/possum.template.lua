-- title:  Possum Zeus
-- author: Zeus3rd@gmail.com
-- desc:   short description
-- script: moon

-- Constant Settings
tile_size=2 -- tile Width/Height
gravity=1.0/60

-- Debug Collissions (old TODO: Remove)
drawColl=true

-- Debug Modes
debug=
  collissions: true

str=(v)->tostring v
round=(n,d)->
  mult=10^(d or 0)
  math.floor(n*mult+0.5)/mult
roundup=(n)->round(n+0.5,0)
rounddown=(n)->round(n-0.5,0)

-- TOdO: OLD
animations=
  possum:
    idle:
      ids:{272,273}
      steps:20
    walk:
      -- ids:{293,295,297,299}
      ids:{336,338,340,342,344,346,348,350}
      steps:5
      w:2
      h:2
    run:
      ids:{293,295,297,299}
      steps:5
      w:2
    jump:
      ids:{276} -- TODO animate
      steps:100
      h:2
    fall:
      ids:{432} -- TODO an actual sprite
      steps:100
      h:2
  rat:
    idle:
      ids:{272,273}
      steps:30

-- TODO:
palettes:
  possum:
    100,200,150
    3,4,5

sprites:
  possum:
    idle:{272,273}
    walk:{293,295,297,299}
    run: {293,295,297,299}
    jump:{276} -- TODO: Incomplete
    fall:{432} -- TODO: Incomplete
  rat: -- TODO: Filler
    idle:{272,273}
    walk:{293,295,297,299}
    run: {293,295,297,299}
    jump:{276}
    fall:{432}

  --TODO: Filler
  mage:{272}
  knight:{272}

states=
  possum:
    idle:{frame_rate:20}
    walk:{frame_rate:10}
    run: {frame_rate:5}
    jump:{frame_rate:100}
    fall:{frame_rate:100}
  rat:
    idle:{frame_rate:20}
    walk:{frame_rate:10}
    run: {frame_rate:5}
    jump:{frame_rate:100}
    fall:{frame_rate:100}

-- TODO:
classes=
  mage:{att:2}
  knight:{att:5}

-- TODO: Rename to isSolidBG
isSolid=(id)->
  if id>=64 and id<=66 then return true
  if id>=80 and id<=82 then return true
  return false

-- TODO AI BRAINS
-- brains=
  -- sit:(0)->return
  -- 
  -- chase:(0)->return
  -- seek:(0)->return
  -- flee:(0)->return

btnAxis=(a,b)->
  if btn(a) and btn(b) then return 0
  if btn(a) then return -1
  if btn(b) then return 1
  return 0

class RectCollider
  new:(x=0,y=0,w=1,h=1)=>
    @x=x
    @y=y
    @w=w
    @h=h
  draw:(col=8)=>rectb(@x,@y,@w,@h,col)
  collide:(B)=>
    if @x>(B.x+B.w) or (@x+@w-1)<B.x or @y>(B.y+B.h) or (@y+@h-1)<B.y
      return false
    return true

class Actor
  new:(type="rat",state="idle",x=0,y=0,alpha=-1,scale=1,flip=0,rotate=0)=>
    -- General
    @type=type
    @state=state
    @x=x
    @y=y

    -- Physics
    @xv=0
    @yv=0

    -- Sprite & Animation
    @spr=0
    @step=0
    @frame=1
    @alpha=alpha
    @scale=scale
    @flip=flip
    @rotate=rotate

    -- Input
    @up=false
    @left=false
    @right=false
    @down=false
    @attack=false
    @defend=false
  getAnim:=>
    @anim=animations[@type] or {}
    @anim=@anim[@state]
    if @anim
      @step+=1
      if @step>=@anim.steps
        @step=0
        @frame+=1
        @spr=@anim.ids[@frame]
      if @frame>#@anim.ids
        @frame=1
        @spr=@anim.ids[@frame]
      @spr=@anim.ids[@frame]
      @w=@anim.w or 1
      @h=@anim.h or 1
  input:=>
    @up=btn(1) -- TODO: Test & Confirm
    @left=btn(2)
    @right=btn(3)
    @down=btn(4) -- TODO: Test & Confirm
    @attack=btn(4)
    @defend=btn(5)

    if btnAxis(2,3)>0 then @flip=0
    if btnAxis(2,3)<0 then @flip=1
  update:=>
    -- TODO
  draw:=>
    getAnim() -- TODO: Remove if dupliate calling in the future
    if @anim
      spr(@spr,@x,@y,@alpha,@scale,@flip,@rotate,@w,@h)
    else
      spr(264,0,0) -- Error Sprite -- TODO: Remove when stable

    -- TODO: Draw collissions in debug mode
    -- if debug.collissions then @coll\draw!
  ai:=>
    -- TODO

-- Old
class Spr
  new:(type="rat",state="idle",x=0,y=0,alpha=-1,scale=1,flip=0,rotate=0)=>
    @id=0
    @type=type
    @state=state
    @x=x
    @y=y
    @xv=0
    @yv=0
    @w=1
    @h=1
    @alpha=alpha
    @scale=scale
    @flip=flip
    @rotate=rotate
    @step=0
    @frame=1
    @jump=false
    @fall=false
    @run=false
    @left=false
    @right=false
    @coll=RectCollider(@x,@y,@w,@y)
  getAnim:=>
    anim=animations[@type] or {}
    anim=anim[@state]
    -- @y=@y+(anim.h or 1)*8-(@anim.h or 1)*8
    if anim and @anim
      h1=anim.h or 1
      h2=@anim.h or 1
      w1=anim.w or 1
      w2=@anim.w or 1
      if @yv>=0 then @y=@y+(h2-h1)*8
      -- if @xv>0 then @x=@x+(w2-w1)*8
      if @yv<0 then @y=@y-(h1-h2)*8
      
      if @flip==0 then @x=@x-(w1-w2)*8
      
    @anim=anim
  setPos:(x,y,w=1,h=1)=>
    @x=x
    @y=y
    @w=w
    @h=h
    @coll.x=@x
    @coll.y=@y
    @coll.w=@w*8
    @coll.h=@h*8
  gravity:=>
    if @jump and not @fall then @yv=-1
    if not @jump and @yv<0 then @yv=0
    
    -- collission up
    if @yv<0 and isSolid(mget((@x+4)/8,(@y-8)/8)) then @yv=0
    
    -- Gravity
    @fall=true
    @yv+=gravity
    
    mx=(@x+8)/8
    my=(@y+8)/8
    
    -- Max Velocities
    if @xv>8 then @xv=8
    if @yv>8 then @yv=8
    if @xv<-8 then @xv=-8
    if @yv<-8 then @yv=-8
    
    -- collission down
    ux=(@x+@w*8/2)/8
    uy=(@y+@h*8)/8
    if @yv>0 and isSolid(mget(ux,uy))
      @yv=0
      @fall=false
    
    -- collission horz
    if @xv>0 and isSolid(mget((@x+(8*@w))/8,(@y)/8)) then @xv=0
    if @xv>0 and isSolid(mget((@x+(8*@w))/8,(@y+@h*4-1)/8)) then @xv=0
    if @xv>0 and isSolid(mget((@x+(8*@w))/8,(@y+@h*8-1)/8)) then @xv=0
    if @xv<0 and isSolid(mget((@x-1)/8,(@y+7)/8)) then @xv=0
    
  setState:=>
    statePrev=@state
    @state="idle"
    if @jump and @yv<0 then @state="jump"
    -- TODO, dangle/hang
    -- TODO, hurt
    elseif @fall and @yv>=0 then @state="fall"
    elseif @xv!=0
      @state="walk"
      if @run then @state="run"
    if @state!=statePrev
      @step=0
      @frame=1
    
    @getAnim!
    
  update:=>
    @gravity!
    @setState!
    
    @setPos(@x+@xv,@y+@yv,@anim.w,@anim.h)
  draw:=>
    if @anim
      @step+=1
      if @step>=@anim.steps
        @step=0
        @frame+=1
        @id=@anim.ids[@frame]
      if @frame>#@anim.ids
        @frame=1
        @id=@anim.ids[@frame]
      @id=@anim.ids[@frame]
      @w=@anim.w or 1
      @h=@anim.h or 1
      spr(@id,@x,@y,@alpha,@scale,@flip,@rotate,@w,@h)
    else
      spr(264,0,0)

    if drawColl then @coll\draw!
  input:=>
    @xv=btnAxis(2,3)

    @jump=btn(4)
    @run=btn(5)
    @left=btn(2)
    @right=btn(3)

    print @right,50,50
    
    if @xv>0 then @flip=0
    elseif @xv<0 then @flip=1
    
    if @run then @xv=@xv*2
  ai:=>
    -- TODO

player=Spr("possum")

-- input=(o)->
  -- o.s="idle"
  -- if btnp 0,60,6 then o.s="up"
  -- if btnp 1,60,6 then o.s="down"
  -- if btnp 2,60,6 then o.s="left"
  -- if btnp 3,60,6 then o.s="right"

-- Palette Swap Func
pal=(i,r,g,b)->
  if i<0 then i=0
  if i>15 then i=15
  if r==nil and g==nil and b==nil then
    return peek(0x3fc0+(i*3)),peek(0x3fc0+(i*3)+1),peek(0x3fc0+(i*3)+2)
  else
    if r==nil or r<0 then r=0
    if g==nil or g<0 then g=0
    if b==nil or b<0 then b=0
    if r>255 then r=255
    if g>255 then g=255
    if b>255 then b=255
    poke 0x3fc0+(i*3)+2,b
    poke 0x3fc0+(i*3)+1,g
    poke 0x3fc0+(i*3),r

-- TODO
camx=0
camy=0
drawmap=->
  map camx,camy,camx+30,camy+17

-- test=0
export TIC=->
  cls(12)
  drawmap!
  player\input!
  player\update!
  player\draw!
  -- input(player)
  -- draw(player)
  
  -- spr 277,50,50,0,1,0,0,2,2
  -- spr 400+test,43,48,0,1
  -- test+=1
  -- if test>5 then test=0
  -- drawmap!
  -- move!
  -- draw!
  -- drawferal 0,100,100