-- title:  Secret
-- author: Zeus
-- desc:   TODO
-- script: moon

Models={'f','r','m'}

Sprites=
  f: 256
  r: 288
  m: 320

class Actor
  new:(x,y,model=math.random(#Models))=>
    @x=x
    @y=y
    @sprite=Sprites[model]
    @state=1
    @rotate=0
    @diag=0
  draw:=>
    sprite=@sprite
    if @diag>0 then sprite+=2
    spr @sprite+@state*2,@x,@y,2,2,-1,1,0,@rotate
  input:=>
    -- TODO: Don't like this method, need to translate to 8-directional logic and have more logical drawing script
    if btn(0)
      @rotate=0
      if btn(1) then @diag=1
    else if btn(1)
      @rotate=1
      if btn(2) then @diag=1
    else if btn(2)
      @rotate=2
      if btn(3) then @diag=1
    else if btn(3)
      @rotate=0
      if btn(0) then @diag=1

function TIC()

  -- if btn(0) then y=y-1 end
  -- if btn(1) then y=y+1 end
  -- if btn(2) then x=x-1 end
  -- if btn(3) then x=x+1 end

  cls(13)
  -- spr(1+t%60//30*2,x,y,14,3,0,0,2,2)
  -- print("HELLO WORLD!",84,84)
  -- t=t+1
end
