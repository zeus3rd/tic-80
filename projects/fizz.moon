-- title:  Fizz Fox
-- author: game developer
-- desc:   Fizz the Wizard Fox
-- script: moon

-- CLI Debugging placeholders (Remove)
poke=poke or =>
poke4=poke4 or =>
spr=spr or =>
mget=mget or =>
mset=mset or =>
map=map or =>
btn=btn or =>
cls=cls or =>
-- CLI Debugging placeholders (Remove)

-- Globals
player=nil
wizards=nil
dialog=nil
palette=nil
bats=nil
world=nil
projectiles=nil


inventory=
	items:
		key_silver:false
		key_gold:false
		key_emerald:false


solidTiles={
	{64,95},
}
doorTiles=
	wood:{{96,72},{112,113}}
	silver:{{130,131},{151,152}}
	gold:{{128,129},{149,150}}
	emerald:{{132,133},{153,154}}

solidTilesEnemy={
	{64,95}
	{96,99}
	{112,115}
	{128,133}
	{144,149}
}

isSolidPlayer=(x,y,forceDoors=false)->
	s=0
	if mget~=nil
		s=mget x,y
	-- TODO: Locked door types check (against inventory key)
	for i,o in pairs solidTiles
		if s>=o[1] and s<=o[2] then return true
	if forceDoors==true or inventory.items.key_silver~=true
		for i1,o1 in pairs doorTiles.silver
			for i2,o in pairs o1
				if s>=o1[1] and s<=o1[2] then return true
	if forceDoors==true or inventory.items.key_gold~=true
		for i1,o1 in pairs doorTiles.gold
			for i2,o in pairs o1
				if s>=o1[1] and s<=o1[2] then return true
	if forceDoors==true or inventory.items.key_emerald~=true
		for i1,o1 in pairs doorTiles.emerald
			for i2,o in pairs o1
				if s>=o1[1] and s<=o1[2] then return true
	return false

isSolidEnemy=(x,y)->
	s=0
	if mget~=nil
		s=mget x,y
	for i,o in pairs solidTilesEnemy
		if s>=o[1] and s<=o[2] then return true

palette=
	name:'default'
	set:(name)=>
		if name~=nil and @name!=name and @[name]~=nil
			@name=name
			@palhex @[name]
	swap:(c0,c1)=>
		if c0==nil and c1==nil
			for i=0,15
				poke4(0x3FF0*2+i,i)
		else poke4 0x3FF0*2+c0,c1
	pal:(i,r,g,b)->
		if i<0 then i=0
		if i>15 then i=15
		if peek==nil then return
		if r==nil and g==nil and b==nil
			return peek(0x3fc0+(i*3)),peek(0x3fc0+(i*3)+1),peek(0x3fc0+(i*3)+2)
		else
			if r==nil or r<0 then r=0
			if g==nil or g<0 then g=0
			if b==nil or b<0 then b=0
			if r>255 then r=255
			if g>255 then g=255
			if b>255 then b=255
			poke 0x3fc0+(i*3)+2,b
			poke 0x3fc0+(i*3)+1,g
			poke 0x3fc0+(i*3),r
	palhex:(str)=>
		l=#str/2
		for i=0,l-1
			s=string.sub str,i*2,i*2+2
			n=tonumber s,16
			poke 0x3fc0+i,n
	-- Color Palettes
	default:'140c1c44243430346d4e4a4e854c30346524d04648757161597dced27d2c8595a16daa2cd2aa996dc2cadad45edeeed6'
	c64:'000000ffffffa8734ae9b287772d26b6686285d4dcc5ffffa85fb4e99df5559e4a92df8742348b7e70cabdcc71ffffb0'
	easter:'f6f6bfe6d1d1868691794765f5e17aedc38dcc8d86ca657e39d4b98dbcd28184ab6860869dc0857ea788567864051625'
-- palette\set 'default'


world=
	x:0,y:0 -- Screen Position
	xd:0,yd:0 -- Screen Slide Destination Position
	w:30,h:17
	sx:0,sy:0
	ckey:0
	scale:1
	moving:false
	posRoom:(x=0,y=0)=>
		@x=x*@w
		@xd=@x
		@y=y*@h
		@yd=@y
	slideRoom:(xd=0,yd=0)=>
		@xd=xd
		@yd=yd
	step:=>
		@move!
		@draw!
	move:=>
		if not @x==@xd or not @y==@yd
			@moving=true
			if @x>@xd then @x-=1
			if @x>@xd then @x+=1
			if @y>@yd then @y-=1
			if @y>@yd then @y+=1
		else @moving=false
	draw:=>
		map @x,@y,@w,@y,@sx,@sy,@ckey,@scale

world\posRoom 6,2


player=
	s:256
	x:0,y:0 -- World Pixel Position
	xs:1 -- Move Speed
	ys:1
	health:100
	jumping:0 -- Jump timer
	colorkey:1
	flip:0
	rotate:0
	w:2,h:2
	tx:0,ty:0
	attackTime:0

	animTime:0
	animFrame:1
	state:'idle'
	states:
		idle:{256}
		fall:{258}
		jump:{258}
		walk:{256,258}
		dead:{260}
	pos:(x=0,y=0)=>@x,@y=x,y
	posMap:(x=0,y=0)=>@x,@y=x*8,y*8
	setState:(state)=>
		if @state!=state
			@animTime=0
			@animFrame=1
		@state=state
	onGround:=>isSolidPlayer(@tx,@ty+@h) or isSolidPlayer(@tx+@w,@ty+@h)
	step:=>
		if dialog.active==nil
			@calcs!
			@move!
			@input!
			@anim!
		@draw!
	die:=>@setState 'dead'
	calcs:=>
		@tx,@ty=math.floor(@x/8),math.floor(@y/8)
		if @attackTime>0 then @attackTime-=1
	attack:=>
		if @state=='dead' then return
		if @attackTime>0 then return
		x=@x+@w*8
		if @flip==1 then x=@x-8 -- TODO: Replace '8' with width of selected projectile

		y=@y+@h*4
		s='fire'
		t=60 -- Cooldown - TODO: Dynamic based on inventory?		
		-- TODO: change attack type depending on inventory items?
		projectiles\add x,y,@flip,s
		@attackTime=t
	move:=>
		if @jumping>0
			@jumping-=1
			@y-=1
			return
		else if not @onGround!
			@y+=@ys
			return -- Gravity stops horizontal movement?
		if @state=='walk' and @flip==1 then @x-=@xs
		if @state=='walk' and @flip==0 then @x+=@xs
		-- if btn 2 then @x-=@xs
		-- if btn 3 then @x+=@xs
	input:=>
		if @state == 'dead'
			-- TODO: on btn 5 or 6, respawn?
			return

		-- Attack
		if btn 4 then @attack!

		-- Direction
		if btn(2) then @flip=1
		if btn(3) then @flip=0

		-- Airborne
		if not @onGround!
			@setState 'fall'
			return

		-- Walk
		if btn(2) or btn(3) then @setState 'walk'

		-- Jump
		if btn(0)
			@setState 'jump'
			@jumping=20
		else @jumping=0

		-- Idle
		if btn()==0 then @setState 'idle'
	anim:=>
		if #@states[@state]==1
			-- Single Frame
			@animTime=0
			@animFrame=1
		else
			-- Multiple Frame
			@animTime+=1
			if @animTime>10
				@animTime=0
				@animFrame+=1
			if @animFrame>#@states[@state] then @animFrame = 1
		@s=@states[@state][@animFrame]
	draw:=>
		xr=@x-(math.floor(@x/8/world.w)*8*world.w)
		yr=@y-(math.floor(@y/8/world.h)*8*world.h)
		spr @s,xr,yr,0,@colorkey,@flip,@rotate,@w,@h

		-- Statuses
		x=4
		y=4
		h=4
		w=50
		rect x,y,w,h,6
		rect x,y,w,h,1

player\posMap 195,47


dialog=
	active:nil
	debug:true
	step:=>
		if @active~=nil
			@display!
			return
		w=world
		p=player
		for i,o in pairs @items
			wx1=w.x*8
			wy1=w.y*8
			wx2=wx1+(w.w*8)
			wy2=wy1+(w.h*8)
			if o.x>=wx1 and o.x<=wx2 and o.y>=wy1 and o.y<=wy2
				@collide o
				@draw o
	-- 221,222,223
	-- 237,238,239
	-- 253,254,255
	display:=>
		if @active==nil then return
		xf=2
		yf=2
		xl=world.w-(xf*2)
		yl=math.ceil (world.h/2)
		-- Draw Background/Border
		for x=xf,xl
			for y=yf,yl
				s=238
				if x==xf and y==yf then s=221
				else if x==xf and y==yl then s=253
				else if x==xl and y==yf then s=223
				else if x==xl and y==yl then s=255
				else if x==xf then s=222
				else if x==xl then s=254
				else if y==yf then s=237
				else if y==yl then s=239
				spr s,x*8,y*8,0
		-- Write Text
		for i,text in pairs @active.text do print text,(xf+2)*8,(yf+1+i)*8
		-- Close dialog
		if btn(4) or btn(5)
			@active.done=true
			@active=nil
	draw:(o)=>
		w=world
		if @debug and not @active
			xr=o.x-(math.floor(o.x/8/w.w)*8*w.w)
			yr=o.y-(math.floor(o.y/8/w.h)*8*w.h)
			c=15
			if o.done~=nil then c=6
			rectb xr,yr,o.w,o.h,c
		-- else if @active
			-- TODO: draw dialog
	collide:(o)=>
		if o.done==true then return
		p=player
		if p.x+p.w>o.x and p.x<o.x+o.w
			if p.y+p.h>o.y and p.y<o.y+o.h
				@active=o
	items:
		awaken:
			x:192*8,y:47*8,w:16,h:16
			text:{'How long have I been asleep?','Blasted sleeping spell','Where are the other initiates','I must be late for class!'}
		destroyed_hallway:
			-- TODO: Relocate to first hallway spawn spot
			x:180*8,y:47*8,w:16,h:16
			text:{'What the? What happened here?','I must have slepped through an attack on the castle'}


portal=
	debug:true
	step:=>
		w=world
		p=player
		for i,o in pairs @items
			wx1=w.x*8
			wy1=w.y*8
			wx2=wx1+(w.w*8)
			wy2=wy1+(w.h*8)
			if o.x>=wx1 and o.x<=wx2 and o.y>=wy1 and o.y<=wy2
				@collide o
				@draw o
				if p.x>o.x and p.x+p.w<o.x+o.w then @active=o
	collide:(o)=>
		if o.done~=nil then return
		p=player
		d=o.dest
		if p.x+p.w>o.x and p.x<o.x+o.w
			if p.y+p.h>o.y and p.y<o.y+o.h
				p\posMap d.x,d.y
				projectiles\clear!
	draw:(o)=>
		w=world
		if @debug and not @active
			xr=o.x-(math.floor(o.x/8/w.w)*8*w.w)
			yr=o.y-(math.floor(o.y/8/w.h)*8*w.h)
			c=15
			if o.done~=nil then c=6
			rectb xr,yr,o.w,o.h,c
	items:
		dorm_l:
			x:180*8,y:47*8,w:16,h:16
			dest:{x:176,y:47}


bats=
	w:8,h:8
	vJump:-60
	items:{}
	-- TODO: Migrate to named states method
	states:{
		{s:352}, -- Flap up
		{s:353,t:10}, -- Flap down
	}
	-- x/y = position
	-- s = @states index
	-- t = timer countdown
	-- d = direction (1/-1)
	add:(x=0,y=0,s=1,t=math.random(1,20),d=1)=>table.insert @items,{x:x,y:y,s:s,t:t,d:d,vX:60,vY:20}
	addMap:(x=0,y=0,s=1,t=math.random(1,20),d=1)=>@add x*8,y*8,s,t,d
	step:=>
		for i,o in pairs @items
			-- TODO: Only work with items within screen
			wx1=world.x*8
			wy1=world.y*8
			wx2=wx1+(world.w*8)
			wy2=wy1+(world.h*8)
			if o.x>=wx1 and o.x<=wx2 and o.y>=wy1 and o.y<=wy2
				if o.vY>=60 then @flap o
				if dialog.active==nil
					@move o
					@animate o
					@collide o
				@draw o
				@collidePlayer o
	flap:(o)=>
		o.s=2
		-- o.vY=@vJump
		o.vY=o.vY*-1
		o.vY+=math.random(0,100)-50
	move:(o)=>
		-- o.vY+=9.8/60 -- Gravital Acceleration
		o.vY+=6
		o.x+=(o.vX/60)*o.d
		o.y+=o.vY/60
	animate:(o)=>
		st=@states[o.s]
		if st.t~=nil then o.t-=1
		if o.t<=0
			o.s=1
			if o.s>#@states then o.s=1
			if st.t~=nil then o.t=st.t
	collide:(o)=>
		-- TODO: Player collision
		-- Map TileaCollision
		tx=math.floor (o.x/8)
		ty=math.floor (o.y/8)
		if isSolidEnemy~=nil
			-- Vertical
			if o.vY>0
				if isSolidEnemy tx,ty+1 then @flap o
			else if o.vY<0
				if isSolidEnemy tx,ty then o.vY=0
			-- Horizontal
			if o.d==1 then tx+=(@w/8)
			if isSolidEnemy tx,ty then o.d=o.d*-1
	collidePlayer:(o)=>
		if o.x+@w>player.x
			if o.x<player.x+player.w*8
				if o.y+@h>player.y
					if o.y<player.y+player.w*8
						player\die!
	draw:(o)=>
		st=@states[o.s]
		flip=0
		if o.d==-1 then flip=1
		xr=o.x-(math.floor(o.x/8/world.w)*8*world.w)
		yr=o.y-(math.floor(o.y/8/world.h)*8*world.h)
		spr st.s,xr,yr,0,1,flip

bats\addMap 199,40 -- Dorm
bats\addMap 211,37 -- Dorm landing

projectiles=
	items:{}
	states:
		fire:{t:20,sp:1,d:10,496,497}
	add:(x=0,y=0,flip=0,s='fire')=>table.insert @items,{x:x,y:y,flip:flip,s:s,t:0,f:1}
	clear:=>items={}
	move:(o)=>
		sp=o.sp or 1
		if o.flip==1 then o.x-=sp
		else o.x+=sp
	anim:(o)=>
		if #@states[o.s]<=1 then return
		t=@states[o.s].t or 30
		if o.t>=t
			o.t=0
			o.f+=1
			if o.f>=#@states[o.s] then o.f=1
	draw:(o)=>
		s=@states[o.s][o.f]
		w=o.w or 1
		h=o.h or 1
		xr=o.x-(math.floor(o.x/8/world.w)*8*world.w)
		yr=o.y-(math.floor(o.y/8/world.h)*8*world.h)
		spr s,xr,yr,0,1,o.flip,0,w,h
	bounds:(i,o)=>
		s=@states[o.s]
		w=s.w or 8
		h=s.h or 8
		p=player
		roomr=(math.ceil player.x/8/world.w)*world.w*8
		rooml=(math.floor player.x/8/world.w)*world.w*8
		-- Screen edge
		if o.x+w>=roomr or o.x<=rooml
			table.remove @items,i
			return true
		-- Solid object
		if isSolidEnemy o.x,o.y,true
			table.remove @items,i
			return true
		if o.x+w>=p.x and o.x<=p.x+p.w*8 and o.y+h>=p.y and o.y<=p.y+p.h*8
			table.remove @items,i
			player\die!
			return true
		if wizards\collideCheckProj o
			table.remove @items,i
			return true

		-- TODO: Wizards
		-- TODO: Bats
		return false
	step:=>
		for i,o in pairs @items
			@move o
			-- Remove projectile if out of bounds
			if @bounds i,o then continue
			@anim o
			@draw o

wizards=
	items:{}
	w:2,h:2
	colorkey:1
	states:
		fox:
			health:10
			idle:{256}
			walk:{256,258}
			attack:{260,t:30,next:'walk'}
			dead:{262} -- TODO: Needs a defined sprite
		wolf:
			health:30
			idle:{288}
			walk:{288,290}
			attack:{292,t:30,next:'walk'}
			dead:{294} -- TODO: Needs a defined sprite
		leopard:
			health:100
			idle:{320}
			walk:{320,322}
			attack:{324,t:30,next:'walk'}
			dead:{326} -- TODO: Needs a defined sprite
	attackTimer:=>return math.random(60,120)
	add:(x=0,y=0,p='default',f=true,t='fox',s='walk')=>table.insert @items,{x:x,y:y,palette:p,flip:f,type:t,state:s,animTime:0,animFrame:1,rotate:0,health:@states[t].health or 10,attack:@attackTimer!}
	addMap:(x=0,y=0,p='default',f=true,t='fox',s='walk')=>@add x*8,y*8,p,f,t,s
	collideCheckProj:(o)=>
		w=o.w or 8
		h=o.h or 8
		hit=false
		for i,wiz in pairs @items
			if wiz.state~='dead' and o.x+w>=wiz.x and o.x<=wiz.x+@w*8 and o.y+h>=wiz.y and o.y<=wiz.y+@h*8
				hit=true
				@hit i,o
		return hit
	hit:(i,proj)=>
		o=@items[i]
		d=projectiles.states[proj.s].d or 0
		o.health-=d
		if o.health<=0 then o.state='dead'
	step:=>
		for i,o in pairs @items
			-- TODO: Only step when within player screen zone
			if dialog.active==nil and o.state~='dead'
				@attack o
				@move o
			@anim o
			@draw o
	attack:(o)=>
		o.attack-=1
		if o.attack<=0
			-- TODO: Launch Projectile
			o.attack=@attackTimer!
			o.state='attack'
	move:(o)=>
		-- Edge of floor turn
		tx=(o.x+1+(8*@w))/8
		if @flip then tx=(o.x-1)/8

		ty1=((o.y+@h*8)/8)
		ty2=((o.y+1+@h*8)/8)

		-- Wall turn
		if isSolidEnemy tx,ty1 then @flip=not @flip

		-- Floor turn
		if not isSolidEnemy tx,ty2 then @flip=not @flip

		-- walk movement
		if o.state=='walk'
			if o.flip then o.x+=1
			else o.x-=1
	anim:(o)=>
		t=@states[o.type][o.state].t or 10
		o.animTime+=1
		if o.animTime>t
			o.animTime=0
			o.animFrame+=1
			o.state=@states[o.type][o.state].next or o.state
		if o.animFrame>#@states[o.type][o.state] then o.animFrame = 1
		o.s=@states[o.type][o.state][o.animFrame]
	draw:(o)=>
		xr=o.x-(math.floor(o.x/8/world.w)*8*world.w)
		yr=o.y-(math.floor(o.y/8/world.h)*8*world.h)
		spr o.s,xr,yr,0,@colorkey,o.flip,o.rotate,@w,@h

wizards\addMap 189,47,nil,nil,'fox'


export TIC=->
	cls(0)
	world\step!
	if not world.moving
		player\step!
		bats\step!
		wizards\step!
		dialog\step!
		portal\step!
		projectiles\step!


-- <TILES>
-- 001:003333330337aa73033aaa73337aaa7337aaaa733aaaaa733777777333333333
-- 002:33333333aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa7777777733333333
-- 003:33333333aaaaaa73aaaaaa73aaaaaa73aaaaaa73aaaaaa737777777333333333
-- 004:333333333aaaaa733aaaaa733aaaaa733aaaaa733aaaaa733777777333333333
-- 005:333333003aaa73303aaaa7303aaaaa333aaaaa733aaaaa733777777333333333
-- 006:3a33333333aa33333333aaa33aa3333aa33a3aa33333a333333a333333a33333
-- 007:3a33333333a3aa33333a33a3333a333aaaa3a3333333a333333a333333a33333
-- 008:00aaaaaaaa33a3333333a33333333aaa333aa33333a3333333a3333333a33333
-- 009:aaaaaa00333333aa333aaaa33aa33333a3a3333333a3333333a3333333a33333
-- 010:0044444444411111111411111111111411111111111114111411111111111111
-- 011:4444444411111111111411111111111411111111111114111411111111111111
-- 012:4444440011111444111411111111111111111111111114111411111111111111
-- 013:005b5bb5b5511111111411111111111411111111111114111411111111111111
-- 014:5b5b5bb511111111111411111111111411111111111114111411111111111111
-- 015:5b5b5b001111115b111411111111111411111111111114111411111111111111
-- 022:3a33333333a3a333333a3aa33333333aa33333333a33333333a3333333a33333
-- 023:3a33333333a3333333a333333a3333aaa333aa33333a333333a3333333a33333
-- 026:1111111111111111111141111111111111111111111114111411111111111111
-- 027:1111111111411111111111411111111111111111111114111111111111111111
-- 028:1111111111111111111111111141111111111111111111111411141111111111
-- 029:1111111111111111114111111111111111111411111111111114111111111111
-- 030:1111111111111111111111411114111111111111111114111111111111111111
-- 031:1111111111111411111111111111111111111111114111111111114111111111
-- 032:4444444449994999499949994444444400040000004000000400000040000000
-- 033:4444444449994999499949994444444400000000000000000000000000000000
-- 034:4444444499944999999449994444444400000000000000000000000000000000
-- 035:4444444499949994999499944444444400000000000000000000000000000000
-- 036:4444444499949994999499944444444400004000000004000000004000000004
-- 096:0000111100114441014444411444444114444441144444411444444114444941
-- 097:1111000014441100144444101444444114444441144444411444444114944441
-- 098:0000111100114441014444411444444114444441144444411444444114444e41
-- 099:1111000014441100144444101444444114444441144444411444444114e44441
-- 100:0000111100114441014444411444444114444441144444411444444114444b41
-- 101:1111000014441100144444101444444114444441144444411444444114b44441
-- 102:0000111100114441014444411444444114444441144444411444444114444a41
-- 103:1111000014441100144444101444444114444441144444411444444114a44441
-- 112:1444494114444941144444411444444114444441144444411444444111111111
-- 113:1494444114944441144444411444444114444441144444411444444111111111
-- 114:14444e4114444e411444444114444e4114444441144444411444444111111111
-- 115:14e4444114e444411444444114e4444114444441144444411444444111111111
-- 116:14444b4114444b411444444114444b4114444441144444411444444111111111
-- 117:14b4444114b444411444444114b4444114444441144444411444444111111111
-- 118:14444a4114444a411444444114444a4114444441144444411444444111111111
-- 119:14a4444114a444411444444114a4444114444441144444411444444111111111
-- 190:7000000770700707707007077070070770700707707007077777777770700707
-- 192:4000000046000000460000004600000046000000460000004600000040000000
-- 193:00006000000060000006a000000aa000000aa000000aa0000e0ee0e000eeee00
-- 194:0000000000000000000000000000000000000000eeeeeeeea777777a0aaaaaa0
-- 195:0000000400000064000000640000006400000064000000640000006400000004
-- 208:4066666004666660043333400400004004000040040000400400004040000004
-- 209:4444444440333333403000004030000040300000403000004030000040300000
-- 210:4444444433333304000003040000030400000304000003040000030400000304
-- 211:0666660406666640043333400400004004000040040000400400004040000004
-- 212:0007777000666666066666664444444443333333400300004003000040000000
-- 213:0777700066666600666666604444444433333334000030040000300400000004
-- 221:0000000000000004044400490499444904999999049999990499999904999999
-- 222:0000000040000044944444999999999999999999999999999999999999999999
-- 223:0000000040004440944499409999994099999940999994009999940099999940
-- 224:0000033300033003003000030300000303000003300000033000000330000003
-- 225:33300000a0033000a0000300a0000030a0000030a0000003a0000003a0000003
-- 226:0000000000000003000000030000000300000000000000030000003300003333
-- 227:0000000000000000000000000000000000000000000000003300000033330033
-- 228:00000009000000090000000e0000000000000009000000090000000e00000000
-- 229:09000000090000009e000000e0000000e9000000090000009e000000e0000000
-- 230:0707070777070070007077007707000000700000707000000700000070000000
-- 231:7070707007007077007707000000707700000700000007070000007000000007
-- 232:000000000000000000060009000a0099000a00990a9990990a00099900000000
-- 233:0011000001110000114100001441000014410000144100001441000014410000
-- 234:0000110000001110000014110000144100001441000014410000144100001441
-- 235:efffffffff222222f8888888f8222222f8fffffff8ff0ffff8ff0ffff8ff0fff
-- 236:fffffeee2222ffee88880fee22280feefff80fff0ff80f0f0ff80f0f0ff80f0f
-- 237:0449999900499999004999990049999900499999004999990499999904999999
-- 238:9999999999999999999999999999999999999999999999999999999999999999
-- 239:9999994099999440999994009999940099999400999994009999940099999400
-- 240:300000033000000330000003300000033000000330aaaaa33aaaaaa333333333
-- 241:a0000003a0000003a0000003a0000003a00000033aaaaa033aaaaaa333333333
-- 242:0000000000000000000000000000000000000000000000003300000033333300
-- 243:0000003300000003000000030000000300000000000000000000000000000000
-- 244:000000090000060960000a0ea9e6eae9ae0a000009eae00e00999eee00000999
-- 245:e9000000090000009e060006e9ea6e9ae000a0ea0e0eae90eee9990099900000
-- 246:00944900000990000009e000000ee006600ee00aa00e900ae009900e99e99e99
-- 247:0000000000000000600ee000a09ee906a009900aa090090a90e00e0909000090
-- 248:0000000000000000900060009900a0009900a000990999a0999000a000000000
-- 249:1441000014410000144100001441000014410000144100001441000011110000
-- 250:0000144100001441000014410000144100001441000014410000144100001111
-- 251:f8fffffff8888888f888f888f8888ffff8888888f2222222ff000fffefffffef
-- 252:fff800ff88880ffef8880fee88880fee88880fee2222ffee000ffeeeffffeeee
-- 253:0499999904999999044999990044999900049999000499990004444400000000
-- 254:9999999999999999999999999999999999999999994444444444400000000000
-- 255:9999940099999400999999409999994099999940444499400044444000000000
-- </TILES>

-- <SPRITES>
-- 000:0000000000000000000000060000000600000006000000060000000600000006
-- 001:00000000000000000000060060006600666666006d666d006fff1f006fffff00
-- 002:000000000000000600000006000000060000000600bb000600bbbb06000bbbbb
-- 003:000000000000060060006600666666006d666d006fff1f006fffff00b5555b00
-- 004:0000000000000000000006000000660000006666000066d6000066ff00f066ff
-- 005:000000000000000000600000066000006660000066d00000f1f00000fff00000
-- 016:0000000b000000bb00ff00bb00ff60bb000f666b000066660000000000000007
-- 017:b5555b00b6666b00666ff00076fff7006ffff0006ffff0006ffff00000000700
-- 018:0000bbbb0000000000000006000f666600ff666600ff60000000000700000000
-- 019:b6666000666ff00076fff7006ffff0006ffff0006ffff0000000070000000000
-- 020:0ff0bb550ffbbb660f6bb666066bb66f0666b6ff006666ff000006ff00000007
-- 021:55b0000066b07000ff500000ff507000ff500000ff000000ff00000000700000
-- 022:0000000000000000000000ff00000fff0007776f00777766bb777766bbb77555
-- 023:0000000000000000fff00000ffff0000f7770000677770006777700055770000
-- 032:0000000000000000000000030000000300000003000000030000000300000003
-- 033:00000000000000000000030030003300333333003d333d003aa11a003aaaaa00
-- 034:000000000000000300000003000000030000000300bb000300bbbb03000bbbbb
-- 035:000000000000030030003300333333003d333d003aa11a003aaaaa00b5555b00
-- 036:0000000000000000000003000000330000003333000033d3000033aa000033aa
-- 037:000000000000000000300000033000003330000033d0000011a00000aaa00000
-- 048:0000000b000000bb000000bb000000bb000330bb000333330000000000000007
-- 049:b5555b00b3333b00333aa00073aaa7003aaaa0003aaaa0003aaaa00000000700
-- 050:0000bbbb00000000000000000003333300033003000000000000000700000000
-- 051:b3333000333aa00073aaa7003aaaa0003aaaa0003aaaa0000000070000000000
-- 052:0000bb55003bbb33033bb333033bb33a033bb3aa003333aa000003aa00000007
-- 053:55b0000033b07000aa500000aa507000aa500000aa000000aa00000000700000
-- 054:0000000000000000000000aa00000aaa0007773a00777733bb777733bbb77555
-- 055:0000000000000000aaa00000aaaa0000a7770000377770003777700055770000
-- 064:00000000000000090000009e0000009e00000099000000090000000900000009
-- 065:000000000000090099999e90e999ee90999999909d999d00eee11e00eeeeee00
-- 066:000000090000009e0000009e000000990000000900bb000900bbbb09000bbbbb
-- 067:0000090099999e90e999ee90999999909d999d00eee11e00eeeeee00b5555b00
-- 068:0000000000000090000009e900009ee9000099990000099d000009ee000009ee
-- 069:0000000000090000999e900099ee900099999000999d0000e11e0000eeee0000
-- 080:0000000b000000bb000000bb099000bb099999bb000999990000000000000004
-- 081:b5555b00b9999b00999ee00049eee4009eeee0009eeee0009eeee00000000400
-- 082:0000bbbb00000000000999000999999009900099000000000000000400000000
-- 083:b9999b00999ee00049eee4009eeee0009eeee0009eeee0000000040000000000
-- 084:00990bb50990bbb90990bb990990bb990999bb9e0099999e0000009e00000000
-- 085:555b0000999b04009ee50000eee50400eee50000eee00000eee0000040040000
-- 086:0000000000000000000000ee00000eee0007779e00777799bb777799bbb77555
-- 087:0000000000000000eee00000eeee0000e7770000977770009777700055770000
-- 096:111100000777100001117100007771c000111771033333700033330000000000
-- 097:00000000000000c0031111710077717001171300777100001110000000000000
-- 238:099000bb9449bbbb9449bbbb9445b88b095bbf8b00bbbffb00b9933300994333
-- 239:bb000990bbbb9449bbbb9449b88b5449b8fbb590bffbbb003349bb0033399500
-- 240:0000000000066e0006666660666ee6666ee666e60666ee600006660000000000
-- 241:00000000000666000666ee606ee666e6666ee6660666666000066e0000000000
-- 253:09000090949bb9499bbbbbb90b8bb8b0093333905bb5533555bbbb5504944940
-- 254:0094333505bb335505bbb55505bbb553055bbbbb0055bbbb004940bb00494000
-- 255:533495003335bb50333bbb5035bbbb50bbbbb550bbbb5500bb04940000049400
-- </SPRITES>

-- <MAP>
-- 034:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000341424142414241424142414241424142414142414241424241424142434000000000000000000000000000000000000000000000000000000000000
-- </MAP>

-- <WAVES>
-- 000:00000000ffffffff00000000ffffffff
-- 001:0123456789abcdeffedcba9876543210
-- 002:0123456789abcdef0123456789abcdef
-- </WAVES>

-- <SFX>
-- 000:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000304000000000
-- </SFX>

-- <PALETTE>
-- 000:140c1c44243430346d4e4a4e854c30346524d04648757161597dced27d2c8595a16daa2cd2aa996dc2cadad45edeeed6
-- </PALETTE>

