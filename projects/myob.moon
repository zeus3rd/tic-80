-- title:  myob
-- author: game developer
-- desc:   short description
-- script: moon

x=(240-(8*16))/2
y=(136-(8*4))/2 - 16

logoText='THE GAME'
xt=(240/2) - (6*(#logoText))/2
yt=110

shake=
  steper:0
  dur:1
  intense:0
  activate: false
  start:(i=4,d=1)=>
    @steper=0
    @dur=d
    @intense=i
    @activate=true
  step:=>
    if @activate~=true then return
    if @steper>=@dur
      @steper=0
      @activate=false
      memset(0x3FF9,0,2)
      return
    poke(0x3FF9,math.random(-@intense,@intense))
    poke(0x3FF9+1,math.random(-@intense,@intense))
    @steper+=1

paper=
  sprites:{256,258,260}
  items:{}
  step:=>
    for i,o in pairs @items
      @update o
      @draw o
  update:(o)=>
    o.y+=1
    if o.y>=200
      o.y=0-math.random(0,500)
      o.x=math.random(0,232)
    o.t+=1
    if o.t>=math.random(30,120)
      o.t=0
      o.f+=1
    if o.f > 3 then o.f=1
  draw:(o)=>
    st=@sprites[o.f]
    spr st,o.x,o.y,0,1,0,0,2,2
  add:=>
    table.insert @items,{x:math.random(0,232),y:0-math.random(0,500),t:0,f:1}

for i=1,30
  paper\add!


drawLogo=->
  spr 200,x,y,0,2,0,0,8,4
  print "THE GAME",xt,yt,14

export TIC=->
  cls 0
  if btnp()~=0
    shake\start 2,30

  shake\step!
  paper\step!
  drawLogo!

-- <TILES>
-- 001:efffffffff222222f8888888f8222222f8fffffff8ff0ffff8ff0ffff8ff0fff
-- 002:fffffeee2222ffee88880fee22280feefff80fff0ff80f0f0ff80f0f0ff80f0f
-- 003:efffffffff222222f8888888f8222222f8fffffff8fffffff8ff0ffff8ff0fff
-- 004:fffffeee2222ffee88880fee22280feefff80ffffff80f0f0ff80f0f0ff80f0f
-- 017:f8fffffff8888888f888f888f8888ffff8888888f2222222ff000fffefffffef
-- 018:fff800ff88880ffef8880fee88880fee88880fee2222ffee000ffeeeffffeeee
-- 019:f8fffffff8888888f888f888f8888ffff8888888f2222222ff000fffefffffef
-- 020:fff800ff88880ffef8880fee88880fee88880fee2222ffee000ffeeeffffeeee
-- 206:0000000000000000000000000000000000ee00000dee0000dedd0000ddee0000
-- 216:000000000000000000000000000bb00000bbbbb00bbbbbbb0bbbbbbbbbbb00bb
-- 217:000000000000000000000000000bc0000bbbbb00bbbbcbb0bbbbbbc0bb00cbbc
-- 218:0000000000000000000000000ccc0000ccccc000bcbcc0000ccccc000bcbcc00
-- 219:0000000000000000000000000000ccc0000ccccc000ccdcd00ccccc000ccdcd0
-- 220:00000000000000000000000000000ddd0000cddd000ddddd00ddcddd0dcddd00
-- 221:000000000000000000000000ddd00000ddde0000ddddd000dddedd0000ddded0
-- 222:dedd0000ddee0000dedd0000ddee000ededd0eeeddeeedeededdeeeeddeeedee
-- 223:000000000000000000000000eee00000eeeee000eeeeee00eeeeeee000eeeee0
-- 232:bbbb00bbbbbb00bbbbbb00bbbbbb00bbbbbb00bbbbbb00bbbbb000bbbb0000bb
-- 233:bb00bbcbbb00cbbcbb00bbcbbb00cbbcbb00bbcbbb00cbbcb000bbc00000cb00
-- 234:00ccccc000bcbcc0000ccccc000bcbcc0000cccc0000bcbc00000ccc00000ccc
-- 235:0ccccc000ccdcd00ccccc000ccdcd000cccc0000cdcd0000ccc00000dcd00000
-- 236:0cddc0000dcdd0000cddc0000dcddd0000ddcddd000ddddd0000cddd00000ddd
-- 237:000edde0000dded0000edde000ddded0dddedd00ddddd000ddde0000ddd00000
-- 238:deddeee0ddeeede0deddeee0ddeeedeededdeeeeddeeedeeded00eeedd00000e
-- 239:000eeee0000eeee0000eeee000eeeee0eeeeeee0eeeeee00eeeee000eee00000
-- 250:00000ccc0000bcbd000bcbcc00bcbcdc0bcbcccc00bcbdcd000bdcd00000cd00
-- 251:cc000000cd000000c0000000d000000000000000000000000000000000000000
-- </TILES>

-- <SPRITES>
-- 000:0000000000aaaaaa00afffff00afffff00afaaaa00afffff00afffff00afaaaa
-- 001:00000000aa000000ffa00000ffaa0000afaaa000fffffa00fffffa00aaaafa00
-- 002:0000000000aaaaaa00afffff00afffff00afaaaa00afffff000affff000afaaa
-- 003:00000000aa000000ffa00000ffaa0000afaaa000fffffa00ffffffa0aaaaafa0
-- 004:0000000000aaaaaa00afffff00afffff00afaaaa00afffff0affffff0afaaaaa
-- 005:00000000aa000000ffa00000ffaa0000afaaa000fffffa00ffffa000aaafa000
-- 016:00afffff00afaaaa00afffff00afaaaa00afffff00afffff00aaaaaa00000000
-- 017:fffffa00aaaffa00fffffa00aaaafa00fffffa00fffffa00aaaaaa0000000000
-- 018:000affff000afaaa000affff000afaaa000affff000affff000aaaaa00000000
-- 019:ffffffa0aaaaffa0ffffffa0aaaaafa0ffffffa0ffffffa0aaaaaaa000000000
-- 020:0affffff0afaaaaa0affffff0afaaaaa0affffff0affffff0aaaaaaa00000000
-- 021:ffffa000aaffa000ffffa000aaafa000ffffa000ffffa000aaaaa00000000000
-- </SPRITES>

-- <WAVES>
-- 000:00000000ffffffff00000000ffffffff
-- 001:0123456789abcdeffedcba9876543210
-- 002:0123456789abcdef0123456789abcdef
-- </WAVES>

-- <SFX>
-- 000:000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000304000000000
-- </SFX>

-- <PALETTE>
-- 000:140c1c44243430346d4e4a4e854c30346524d04648757161597dced27d2c8595a15000b67904b6aa0091fa1c81deeed6
-- </PALETTE>

