-- title:  game title
-- author: game developer
-- desc:   short description
-- script: moon

solidTileRanges={
	{1,31},
}

-- STARTSCRIPT
-- FILE: script.isSolid.lua
-- ENDSCRIPT

-- STARTSCRIPT
-- FILE: actor.bats.lua
-- ENDSCRIPT

-- RandoBats!
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)

-- STARTSCRIPT
-- FILE: player.sidescroll.lua
-- ENDSCRIPT

player\pos 4*8,11*8
player.states=
	idle:
		c:0
		t:20
		s:{272}
	idle_l:
		c:0
		t:20
		s:{272}
		f:1
	jump:
		c:0
		t:60
		s:{258}
	jump_l:
		c:0
		t:60
		s:{258}
		f:1
	walk:
		c:0
		t:6
		s:{273,274,275,276}
	walk_l:
		c:0
		t:6
		s:{273,274,275,276}
		f:1

-- STARTSCRIPT
-- FILE: appendages.lua
-- ENDSCRIPT

appendages\add
	target:player
	states:
		idle:
			x:-8
			t:player.states.idle.t
			s:{288}
		idle_l:
			x:8
			f:1
			t:player.states.idle_l.t
			s:{288}
		walk:
			x:-5
			t:player.states.walk.t
			s:{289,290,291,292}
		walk_l:
			x:5
			f:1
			t:player.states.walk_l.t
			s:{289,290,291,292}

flipCheck=(s)->if (string.find player.state,'_l')~=nil then "#{s}_l" else s
jumpCheck==>(string.find player.state,'jump')==nil

input_r==>player\setState 'walk',1
input_l==>player\setState 'walk_l',-1
input_a==>if player.vy==0 then player\setState flipCheck('jump'),nil,4
input_none==>if jumpCheck! then player\setState flipCheck('idle'),0

-- STARTSCRIPT
-- FILE: script.input.lua
-- ENDSCRIPT


drawMap=->
	map 0,0,30,17,0,0,0

export TIC=->
	cls(0)
	input\step!
	drawMap!
	bats\step!
	player\step!
	appendages\step!
