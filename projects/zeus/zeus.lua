-- title:  game title
-- author: game developer
-- desc:   short description
-- script: moon

solidTileRanges={
	{1,31},
}

-- STARTSCRIPT
-- FILE: script.isSolid.lua
-- STARTCODE
isSolid=(x,y)->
	s=0
	if mget~=nil
		s=mget x,y
	for i,sprites in pairs solidTileRanges
		if s>=sprites[1] and s<=sprites[2] then return true
		return false
-- ENDCODE
-- ENDSCRIPT

-- STARTSCRIPT
-- FILE: actor.bats.lua
-- STARTCODE
bats=
	w:8
	h:8
	vJump:-60
	items:{}
	-- TODO: Migrate to states method used in player.sidescroll.lua
	states:{
		{s:320}, -- Flap up
		{s:321,t:10}, -- Flap down
	}
	-- x/y = position
	-- s = @states index
	-- t = timer countdown
	-- d = direction (1/-1)
	add:(x=0,y=0,s=1,t=math.random(1,20),d=1)=>
		item={x:x,y:y,s:s,t:t,d:d,vX:60,vY:20}
		table.insert @items,item
	step:=>
		for i,o in pairs @items
			if o.vY>=60 then @flap o
			@move o
			@animate o
			@collide o
			@draw o

	flap:(o)=>
		o.s=2
		-- o.vY=@vJump
		o.vY=o.vY*-1
		o.vY+=math.random(0,100)-50
	move:(o)=>
		-- o.vY+=9.8/60 -- Gravital Acceleration
		o.vY+=6
		o.x+=(o.vX/60)*o.d
		o.y+=o.vY/60
	animate:(o)=>
		st=@states[o.s]
		if st.t~=nil then o.t-=1
		if o.t<=0
			o.s=1
			if o.s>#@states then o.s=1
			if st.t~=nil then o.t=st.t
	collide:(o)=>
		-- TODO: Player collision
		-- Map TileaCollision
		tx=math.floor (o.x/8)
		ty=math.floor (o.y/8)
		if isSolid~=nil
			-- Vertical
			if o.vY>0
				if isSolid tx,ty+1 then @flap o
			else if o.vY<0
				if isSolid tx,ty then o.vY=0
			-- Horizontal
			if o.d==1 then tx+=(@w/8)
			if isSolid tx,ty then o.d=o.d*-1
	draw:(o)=>
		st=@states[o.s]
		flip=0
		if o.d==-1 then flip=1
		if spr~=nil then spr st.s,o.x,o.y,0,1,flip
-- ENDCODE
-- ENDSCRIPT

-- RandoBats!
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)
bats\add math.random(8,23*8),math.random(16,7*8)

-- STARTSCRIPT
-- FILE: player.sidescroll.lua
-- STARTCODE
player=
	x:0
	y:0
	w:8
	h:8
	vX:0
	vY:0
	t:1
	f:1
	state:'idle'
	states:
		idle:
			c:0
			f:0
			t:40
			s:{256}
			next:'idle'
	pos:(x,y)=>
		@x=x
		@y=y
	setState:(s,vX,vY)=>
		@state=s or @state
		@vX=vX or @vX
		@vY=vY or @vY
	-- jump:=>
	-- 	-- TODO: Jump directly upwards, higher
	-- hop:(dir=1)=>
	-- 	if @state!="idle" then return
	-- 	@state="airborne"
	-- 	@d=dir
	-- 	@vY=-4
	-- 	@vX=dir*1

	step:=>
		@move!
		-- @physics!
		@draw!
	move:=>
		-- if @state=='airborne'
		-- 	if @vY >4 then @vY=4
		@x+=@vX
		@y+=@vY

		-- Tile Above

		-- @vY+=1 -- Gravity Acceleration
	-- physics:=>
	-- 	tx=math.floor (@x/8)
	-- 	ty=math.floor (@y/8)
	-- 	if isSolid~=nil
	-- 		-- Vertical
	-- 		if isSolid tx,ty+1
	-- 			@state="idle"
	-- 			@x=tx*8
	-- 			@y=ty*8
	-- 			@vY=0
	-- 			@vX=0
	-- 			if @d<0 then @x+=1
	-- 			return
	-- 		else @state="airborne"

	-- 		-- if isSolid tx,ty then @vY=0

	-- 		-- Horizontal
	-- 		if @d==1 then tx+=(@w/8)
	-- 		if isSolid tx,ty
	-- 			@state="idle"
	-- 			@vX=0
	draw:=>
		st=@states[@state]
		if st~=nil
			c=st.c or 0
			s=st.s[@f] or 256
			sc=st.sc or 1
			r=st.r or 0
			f=st.f or 0
			t=st.t or 10
			n=st.next or @state or 'idle'
			w=1
			h=1
			-- Draw
			-- TODO: Draw relevant to map scroll pos if applicable (and do-able)
			if spr~=nil then spr s,@x,@y,c,sc,f,r,w,h
			-- Next Frame/State
			@t+=1
			if @t>t
				@f+=1
				@t=1
			if @f>#st.s
				@f=1
				@state=n
-- ENDCODE
-- ENDSCRIPT

player\pos 4*8,11*8
player.states=
	idle:
		c:0
		t:20
		s:{272}
	idle_l:
		c:0
		t:20
		s:{272}
		f:1
	jump:
		c:0
		t:60
		s:{258}
	jump_l:
		c:0
		t:60
		s:{258}
		f:1
	walk:
		c:0
		t:6
		s:{273,274,275,276}
	walk_l:
		c:0
		t:6
		s:{273,274,275,276}
		f:1

-- STARTSCRIPT
-- FILE: appendages.lua
-- STARTCODE
appendages=
	items:{}
	add:(item={})=>
		-- Enforce default values
		item.target=item.target or player or {x:0,y:0}
		item.states=item.states or {}
		item.f=item.f or 1
		item.t=item.t or 0
		for i,o in pairs item.states
			o.x=o.x or 0
			o.y=o.y or 0
			o.c=o.c or 0
			o.r=o.r or 0
			o.f=o.f or 0
			o.t=o.t or 60
			o.sc=o.sc or 1
			o.s=o.s or {511}
		table.insert @items,item
	clear:=>
		for i in pairs @items
			@items[i]=nil
	draw:=>
		for i,o in pairs @items
			if o.target~=nil and o.target.state~=nil
				st=o.states[o.target.state]
				-- Check for state changes
				-- TODO: This doesn't yet work
				-- if o.laststate~=o.target.state
				-- 	o.laststate=o.target.state
					-- o.f=0
					-- o.t=0
				-- Draw/Animate
				if st~=nil
					x=o.target.x+st.x
					y=o.target.y+st.y
					c=st.c or 0
					s=st.s[o.f] or 511
					sc=st.sc or 1
					r=st.r or 0
					f=st.f or 0
					t=st.t or 10
					n=st.next or @state or 'idle'
					w=1
					h=1
					if spr~=nil then spr s,x,y,c,sc,f,r,w,h
					-- Next Frame
					o.t+=1
					if o.t>t
						o.f+=1
						o.t=1
					if o.f>#st.s
						o.f=1
						o.state=n
	step:=>
		@draw!
-- ENDCODE
-- ENDSCRIPT

appendages\add
	target:player
	states:
		idle:
			x:-8
			t:player.states.idle.t
			s:{288}
		idle_l:
			x:8
			f:1
			t:player.states.idle_l.t
			s:{288}
		walk:
			x:-5
			t:player.states.walk.t
			s:{289,290,291,292}
		walk_l:
			x:5
			f:1
			t:player.states.walk_l.t
			s:{289,290,291,292}

flipCheck=(s)->if (string.find player.state,'_l')~=nil then "#{s}_l" else s
jumpCheck==>(string.find player.state,'jump')==nil

input_r==>player\setState 'walk',1
input_l==>player\setState 'walk_l',-1
input_a==>if player.vy==0 then player\setState flipCheck('jump'),nil,4
input_none==>if jumpCheck! then player\setState flipCheck('idle'),0

-- STARTSCRIPT
-- FILE: script.input.lua
-- STARTCODE
input=
	-- Configuration:
	u:=>if input_u~=nil then input_u!
	d:=>if input_d~=nil then input_d!
	l:=>if input_l~=nil then input_l!
	r:=>if input_r~=nil then input_r!
	a:=>if input_a~=nil then input_a!
	b:=>if input_b~=nil then input_b!
	none:=>if input_none~=nil then input_none!
	step:=>
		if btnp~=nil
			if btnp 0,0,1 then @u!
			if btnp 1,0,1 then @d!
			if btnp 2,0,1 then @l!
			if btnp 3,0,1 then @r!
			if btnp 4,0,1 then @a!
			if btnp 5,0,1 then @b!
			n=false
			for i=0,5 do
				n=n or btn i
			if n==false then @none!
-- ENDCODE
-- ENDSCRIPT


drawMap=->
	map 0,0,30,17,0,0,0

export TIC=->
	cls(0)
	input\step!
	drawMap!
	bats\step!
	player\step!
	appendages\step!
