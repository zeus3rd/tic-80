-- title:  Fizz the Fox
-- author: zeus3rd@gmail.com
-- desc:   short description
-- script: moon

solidTileRanges={
	{1,31},
}

-- STARTSCRIPT
-- FILE: script.isSolid.lua
-- STARTCODE
isSolid=(x,y)->
	s=0
	if mget~=nil
		s=mget x,y
	for i,sprites in pairs solidTileRanges
		if s>=sprites[1] and s<=sprites[2] then return true
		return false
-- ENDCODE
-- ENDSCRIPT

player=
	s:2
	x:0,y:0
	xs:1 -- Move Speed
	colorkey:1
	flip:0
	rotate:0
	w:2,h:2

	tx:0,ty:0
	pos:(x=0,y=0)=>
		@x=x
		@y=y
	onGround:=>isSolid(@tx,@ty+@h) or isSolid(@tx+@w,@ty+@h)
	step:=>
		@calcs!
		@move!
		@anim!
		@draw!
	calcs:=>
		@tx=math.floor (@x/8)
		@ty=math.floor (@y/8)
	move:=>
		if isSolid==nil then return
		if not @onGround!
			@y+=1
			return -- Gravity stops horizontal movement?
		if btn 2 then @x-=@xs
		if btn 3 then @x+=@xs
	anim:=>
		-- Direction
		if btn 2 then @flip=1
		if btn 3 then @flip=0

		-- Airborne
		if not @onGround!
			@s=260
			return

		-- Walk
		if btn(2) or btn(3) then @s=258

		-- Idle
		if btn()==0 then @s=256
	draw:=>
		-- TODO: Sprite number based on current action
		spr @s,@x,@y,0,@colorkey,@flip,@rotate,@w,@h

drawMap=->
	map 0,0,30,17,0,0,0

export TIC=->
	cls(0)
	drawMap!
	player\step!
